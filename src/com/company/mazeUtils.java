package com.company;

import java.util.Vector;

class frontPoint{
    public int x, y;

    public frontPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
}

class node{
    public int x, y;
    public boolean isGood = false;
    public int myNumber;
    public Vector<Integer> connectedWith = new Vector();

    public node(int x, int y, int num){
        this.x = x;
        this.y = y;
        this.myNumber = num;
    }
}

public class mazeUtils {

    private int[][] maze;
    private int[][] visited;
    private int[][][] paths;

    private int fieldWidth;
    private int fieldHeight;

    private int cursorX = 0, cursorY = 0;

    private Vector<node> nodes = new Vector();

    public mazeUtils(int [][] maze){
        this.maze = this.visited = maze;

        this.fieldWidth = maze[0].length - 1;
        this.fieldHeight = maze.length - 1;

        this.nodes.add(new node(0, 0, 0));
    }

    public int[][][] findAllPaths(){
        this.prepareField();
        this.drawField();

        this.findNodes();


        return this.paths;
    }

    private void findNodes(){
        if(this.cursorY == this.fieldHeight + 1) {

            this.nodes.add(new node(this.fieldWidth, this.fieldHeight, this.nodes.size()));

            for (node node : this.nodes) {
                System.out.println("Node number: " + node.myNumber + " Coordinates: " + node.x + " " + node.y);
            }
            return;
        }

        //Debug Part
        //System.out.println("X: " + this.cursorX + " Y: " + this.cursorY);
        //System.out.println(this.nodes.size());
        //System.out.println("field width: " + this.fieldWidth);
        //System.out.println("field height: " + this.fieldHeight);

        int goodNeightbors = 0;

        if(this.maze[this.cursorY][this.cursorX] == 0) {
            if (isMoveGood(this.cursorX - 1, this.cursorY))
                goodNeightbors++;

            if (isMoveGood(this.cursorX + 1, this.cursorY))
                goodNeightbors++;

            if (this.isMoveGood(this.cursorX, this.cursorY - 1))
                goodNeightbors++;

            if (this.isMoveGood(this.cursorX, this.cursorY + 1))
                goodNeightbors++;

            if (goodNeightbors > 2)
                this.nodes.add(new node(this.cursorX, this.cursorY, this.nodes.size()));
        }

        if(this.cursorX == this.fieldWidth){
            this.cursorX = 0;
            this.cursorY++;
        }
        else this.cursorX++;


        this.findNodes();
    }

    private boolean isMoveGood(int x, int y){
        if(x < 0 || x > this.fieldWidth)
            return false;

        if(y < 0 || y > this.fieldHeight)
            return false;

        return this.maze[y][x] == 0 && this.visited[y][x] == 0;
    }

    private void prepareField(){
        for(int i = 0; i < this.fieldHeight; i++) {
            for (int y = 0; y <= this.fieldHeight; y++) {
                for (int x = 0; x <= this.fieldWidth; x++) {
                    if (!(x == 0 && y == 0 || x == this.fieldWidth && y == this.fieldHeight)) {
                        int goodNeightbors = 0;

                        if (isMoveGood(x - 1, y))
                            goodNeightbors++;

                        if (isMoveGood(x + 1, y))
                            goodNeightbors++;

                        if (this.isMoveGood(x, y - 1))
                            goodNeightbors++;

                        if (this.isMoveGood(x, y + 1))
                            goodNeightbors++;

                        if (goodNeightbors < 2)
                            this.maze[y][x] = 1;
                    }
                }
            }
        }
    }

    private void drawField(){
        for (int y = 0; y <= this.fieldHeight; y++) {
            for (int x = 0; x <= this.fieldWidth; x++) {
                System.out.print(this.maze[y][x] + " ");
            }
            System.out.println();
        }
    }
}
