package com.company;

public class Main {

    public static void main(String[] args) {
        int[][] e = {
                {0, 0, 0, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 1, 0},
                {0, 1, 1, 0, 1, 1, 1, 0},
                {0, 0, 1, 0, 0, 0, 0, 1},
                {1, 0, 1, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 0}
        };

        mazeUtils mu = new mazeUtils(e);
        mu.findAllPaths();
    }
}
